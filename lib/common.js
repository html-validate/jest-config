const commonConfig = {
	collectCoverage: true,
	collectCoverageFrom: [
		"src/**/*.[jt]s",
		"packages/*/*src/**/*.[jt]s",
		"!**/*.spec.[jt]s",
		"!**/*.d.ts",
		"!**/index.[jt]s",
		"!**/__fixtures__/**",
		"!**/generated/**",
	],
	coverageReporters: ["text", "text-summary", "cobertura", "html", "json", "lcov"],

	/* until jest 30 we must force prettier 2 or formatting snapshots wont work */
	prettierPath: require.resolve("prettier-2"),

	reporters: [
		"default",
		[
			require.resolve("jest-junit"),
			{
				outputDirectory: "temp",
				outputName: "./jest.xml",
			},
		],
	],
	snapshotFormat: {
		escapeString: false,
		printBasicPrototype: false,
	},
	snapshotSerializers: [require.resolve("jest-serializer-ansi-escapes")],
	testEnvironmentOptions: {
		customExportConditions: ["node"],
	},
	testPathIgnorePatterns: [
		"/node_modules/",
		"<rootDir>/build/",
		"<rootDir>/dist/",
		"<rootDir>/cypress/",
		"<rootDir>/packages/.*/dist/",
	],
	testRegex: "(/__tests__/.+|(\\.|/)(test|spec))\\.[jt]s$",
};

module.exports = { commonConfig };
