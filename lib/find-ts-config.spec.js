const { findTsConfig } = require("./find-ts-config");
const fs = require("fs");
const path = require("path");

jest.mock("fs");

function mockFilesystem(files) {
	jest.spyOn(fs, "existsSync").mockImplementation((filePath) => {
		if (filePath.includes("node_modules") || path.dirname(filePath) === __dirname) {
			return true;
		}
		return files.includes(filePath);
	});
}

afterEach(() => {
	jest.restoreAllMocks();
});

it("should find tsconfig.jest.json", () => {
	expect.assertions(1);
	mockFilesystem(["/path/to/project/tsconfig.jest.json"]);
	expect(findTsConfig("/path/to/project")).toBe("/path/to/project/tsconfig.jest.json");
});

it("should find tsconfig.json", () => {
	expect.assertions(1);
	mockFilesystem(["/path/to/project/tsconfig.json"]);
	expect(findTsConfig("/path/to/project")).toBe("/path/to/project/tsconfig.json");
});

it("should prefer tsconfig.jest.json over tsconfig.json", () => {
	expect.assertions(1);
	mockFilesystem(["/path/to/project/tsconfig.jest.json", "/path/to/project/tsconfig.json"]);
	expect(findTsConfig("/path/to/project")).toBe("/path/to/project/tsconfig.jest.json");
});

it("should find from parent directory", () => {
	expect.assertions(1);
	mockFilesystem(["/path/to/project/tsconfig.jest.json"]);
	expect(findTsConfig("/path/to/project/deep/path")).toBe("/path/to/project/tsconfig.jest.json");
});

it("should return undefined if no config file was found", () => {
	expect.assertions(1);
	mockFilesystem([]);
	expect(findTsConfig("/path/to/project")).toBeUndefined();
});
