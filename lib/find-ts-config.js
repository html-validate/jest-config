const fs = require("fs");
const path = require("path");

const search = ["tsconfig.jest.json", "tsconfig.json"];

/**
 * @param {string} dir
 * @returns {string | undefined}
 */
function findMatch(dir) {
	return search.find((it) => {
		const filePath = path.join(dir, it);
		return fs.existsSync(filePath);
	});
}

/**
 * Traverses directory structure looking for typescript configuration file.
 *
 * In order it searches for:
 *
 * - `tsconfig.jest.json`
 * - `tsconfig.json`
 *
 * @param {string} cwd
 * @returns {string | undefined} Absolute path to tsconfig or `undefined` if no match was found.
 */
function findTsConfig(cwd) {
	let current = cwd;

	// eslint-disable-next-line no-constant-condition
	while (true) {
		const match = findMatch(current);
		if (match) {
			return path.join(current, match);
		}

		/* get the parent directory */
		const child = current;
		current = path.dirname(current);

		/* stop if this is the root directory */
		if (current === child) {
			break;
		}
	}

	return undefined;
}

module.exports = { findTsConfig };
