const { commonConfig } = require("./common");
const { detectBabel } = require("./detect-babel");
const { findTsConfig } = require("./find-ts-config");

module.exports = { commonConfig, detectBabel, findTsConfig };
