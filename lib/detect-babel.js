/**
 * Autodetects if babel is present.
 *
 * @internal
 * @returns {boolean}
 */
function detectBabel() {
	try {
		require("@babel/core");
		return true;
	} catch (err) {
		return false;
	}
}

module.exports = { detectBabel };
