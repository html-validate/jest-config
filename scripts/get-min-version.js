const { minVersion, minSatisfying } = require("semver");
const {
	jest,
	"babel-jest": babelJest,
	"ts-jest": tsJest,
} = require("../package.json").peerDependencies;

const constraint = process.argv[2];
const pkgname = process.argv[3] || "";
const minRequiredJest = jest.split("||").map((it) => minVersion(it.trim()));
const minRequiredBabelJest = babelJest.split("||").map((it) => minVersion(it.trim()));
const minRequiredTSJest = tsJest.split("||").map((it) => minVersion(it.trim()));
const foundJest = minSatisfying(minRequiredJest, constraint);
const foundBabelJest = minSatisfying(minRequiredBabelJest, constraint);
const foundTSJest = minSatisfying(minRequiredTSJest, constraint);

const types = {
	25: "25",
	26: "26",
	27: "27",
	28: "27", // ts-jest@28.0.0 requires @types/jest@27
	29: "29",
};

const typescript = {
	25: "3.9",
	26: "3.9",
	27: "4.6",
	28: "4.6",
	29: "4.7",
};

if (!foundJest) {
	process.stderr.write(`Failed to find a jest version that satisfies "${constraint}"\n`);
	process.stderr.write(`The current peerDependency allows: "${jest}"\n`);
	process.exit(1); // eslint-disable-line no-process-exit
}

if (!foundBabelJest) {
	process.stderr.write(`Failed to find a babel-jest version that satisfies "${constraint}"\n`);
	process.stderr.write(`The current peerDependency allows: "${babelJest}"\n`);
	process.exit(1); // eslint-disable-line no-process-exit
}

if (!foundTSJest) {
	process.stderr.write(`Failed to find a ts-jest version that satisfies "${constraint}"\n`);
	process.stderr.write(`The current peerDependency allows: "${tsJest}"\n`);
	process.exit(1); // eslint-disable-line no-process-exit
}

if (!typescript[constraint]) {
	process.stderr.write(`Failed to find a typescript version that matches jest "${constraint}"\n`);
	process.exit(1); // eslint-disable-line no-process-exit
}

switch (pkgname) {
	case "":
		break;
	case "jest":
		process.stdout.write(`${foundJest.version}\n`);
		break;
	case "babel-jest":
		process.stdout.write(`${foundBabelJest.version}\n`);
		break;
	case "ts-jest":
		process.stdout.write(`${foundTSJest.version}\n`);
		break;
	case "typescript":
		process.stdout.write(`${typescript[constraint]}\n`);
		break;
	case "@types/jest":
		process.stdout.write(`${types[constraint]}\n`);
		break;
	default:
		process.stderr.write(`Don't know how to handle package "${pkgname}"\n`);
		process.exit(1); // eslint-disable-line no-process-exit
}
