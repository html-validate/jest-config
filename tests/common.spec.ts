import markup from "./__fixtures__/foo.html";

it("should import html as string", () => {
	expect.assertions(1);
	expect(markup).toMatchInlineSnapshot(`
		"<div>
			<p>lorem ipsum</p>
		</div>
		"
	`);
});
