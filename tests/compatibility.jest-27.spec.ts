import { major } from "semver";
import { add } from "./compatibility";

const { version } = require("jest/package.json");
const jestMajor = major(version);
const testMajor = 27;

function getRandomInt(min: number, max: number): number {
	return Math.floor(Math.random() * (max - min) + min);
}

if (jestMajor === testMajor) {
	it("should add numbers together", () => {
		expect.assertions(1);
		const a = getRandomInt(0, 10);
		const b = getRandomInt(0, 10);
		expect(add(a, b)).toEqual(a + b);
	});

	it("should handle snapshots", () => {
		expect.assertions(4);
		expect(1).toMatchInlineSnapshot(`1`);
		expect("foo").toMatchInlineSnapshot(`"foo"`);
		expect([1, 2, 3]).toMatchInlineSnapshot(`
			[
			  1,
			  2,
			  3,
			]
		`);
		expect({ foo: 'bar "baz"' }).toMatchInlineSnapshot(`
			{
			  "foo": "bar "baz"",
			}
		`);
	});
} else {
	console.log(
		`Skipping compatibility tests for Jest v${testMajor} when running under v${jestMajor}`,
	);
	it("dummy test for mismatched jest version", () => {
		expect.assertions(1);
		expect(true).toBeTruthy();
	});
}
