const { version: tsjestVersion } = require("ts-jest/package.json");
const { commonConfig, detectBabel, findTsConfig } = require("../../lib");

const cwd = process.cwd();
const tsjestMajor = parseInt(tsjestVersion.split(".")[0], 10);
const useLegacyTsJestConfig = tsjestMajor < 29;

const tsjestOptions = {
	stringifyContentPathRegex: "\\.html?$",
	tsconfig: findTsConfig(cwd),
};

const preset = { ...commonConfig };

if (useLegacyTsJestConfig) {
	preset.globals = {
		"ts-jest": tsjestOptions,
	};
	preset.transform = {
		"^.+\\.(tsx?|html)$": require.resolve("ts-jest"),
	};
} else {
	preset.transform = {
		"^.+\\.(tsx?|html)$": [require.resolve("ts-jest"), tsjestOptions],
	};
}

if (detectBabel()) {
	preset.transformIgnorePatterns = [];
	preset.transform["^.+\\.m?js$"] = [
		require.resolve("babel-jest"),
		{
			plugins: [
				[require.resolve("babel-plugin-transform-import-meta"), { module: "ES6" }],
				require.resolve("@babel/plugin-transform-modules-commonjs"),
			],
		},
	];
}

if (require.main === module) {
	/* eslint-disable-next-line no-console */
	console.log(JSON.stringify(preset, null, 2));
}

module.exports = preset;
