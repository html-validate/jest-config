const { commonConfig, findTsConfig } = require("../../lib");

const cwd = process.cwd();

const tsjestOptions = {
	stringifyContentPathRegex: "\\.html?$",
	tsconfig: findTsConfig(cwd),
	useESM: true,
};

const preset = {
	...commonConfig,
	extensionsToTreatAsEsm: [".ts", ".tsx", ".mts"],
	transform: {
		"^.+\\.(tsx?|html)$": [require.resolve("ts-jest"), tsjestOptions],
	},
};

if (require.main === module) {
	/* eslint-disable-next-line no-console */
	console.log(JSON.stringify(preset, null, 2));
}

module.exports = preset;
