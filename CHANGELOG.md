# @html-validate/jest-config changelog

## 3.12.0 (2025-02-16)

### Features

- **deps:** update dependency jest-serializer-ansi-escapes to v4 ([eb8eeb4](https://gitlab.com/html-validate/jest-config/commit/eb8eeb41c3ca729ca67cc9a6b903c544a7a18d32))

## 3.11.2 (2025-01-05)

### Bug Fixes

- **deps:** update dependency babel-plugin-transform-import-meta to v2.3.2 ([8843e62](https://gitlab.com/html-validate/jest-config/commit/8843e62dd893c77b0d2e9b2aca4fcf242be381a7))

## 3.11.1 (2024-12-08)

### Bug Fixes

- **deps:** update dependency @babel/plugin-transform-modules-commonjs to v7.26.3 ([7eaab74](https://gitlab.com/html-validate/jest-config/commit/7eaab742df5c697964d38b1c23ff93405e1af64c))

## 3.11.0 (2024-12-01)

### Features

- **deps:** require nodejs 16 or later ([b078daf](https://gitlab.com/html-validate/jest-config/commit/b078daf2aaeecff437a1c1db922a9004803ffe8e))

### Bug Fixes

- **deps:** bump min typescript versions ([9ca9e58](https://gitlab.com/html-validate/jest-config/commit/9ca9e58ccfa69608c03dd54c61728743570744a3))

## 3.10.0 (2024-04-05)

### Features

- experimental ESM support ([ac9a07e](https://gitlab.com/html-validate/jest-config/commit/ac9a07eecfcfbdfc1d3776e4d54b693d804df324))

## [3.9.0](https://gitlab.com/html-validate/jest-config/compare/v3.8.0...v3.9.0) (2024-2-25)

### Features

- **deps:** update dependency jest-serializer-ansi-escapes to v3 ([28ed241](https://gitlab.com/html-validate/jest-config/commit/28ed241502619f589c594ed1cf0e07e7062ce2b0))

## [3.8.0](https://gitlab.com/html-validate/jest-config/compare/v3.7.7...v3.8.0) (2024-2-11)

### Features

- force prettier 2 for formatting snapshots ([3af3f87](https://gitlab.com/html-validate/jest-config/commit/3af3f879e01824fe6fa0bd8facb6a8731e74b45e))

## [3.7.7](https://gitlab.com/html-validate/jest-config/compare/v3.7.6...v3.7.7) (2023-08-06)

### Dependency upgrades

- **deps:** update dependency babel-plugin-transform-import-meta to v2.2.1 ([2800ae3](https://gitlab.com/html-validate/jest-config/commit/2800ae378ed9a32e51fdd056b23fadc894014ad7))

## [3.7.6](https://gitlab.com/html-validate/jest-config/compare/v3.7.5...v3.7.6) (2023-06-10)

### Bug Fixes

- default to use "node" as export condition ([4de5e03](https://gitlab.com/html-validate/jest-config/commit/4de5e0388c6aa6534036f285a9dd7119ea1b0b7e))

## [3.7.5](https://gitlab.com/html-validate/jest-config/compare/v3.7.4...v3.7.5) (2023-04-23)

### Dependency upgrades

- **deps:** update dependency jest-junit to v16 ([f755cb0](https://gitlab.com/html-validate/jest-config/commit/f755cb01ab15c432fc4de2004564261b7f596976))

## [3.7.4](https://gitlab.com/html-validate/jest-config/compare/v3.7.3...v3.7.4) (2023-04-16)

### Bug Fixes

- fix broken dependency ([a51f8df](https://gitlab.com/html-validate/jest-config/commit/a51f8df02e9375f187038657b40cb22c49ba211c))

## [3.7.3](https://gitlab.com/html-validate/jest-config/compare/v3.7.2...v3.7.3) (2023-04-15)

### Bug Fixes

- transform `import.meta.url` in es modules ([8117fb9](https://gitlab.com/html-validate/jest-config/commit/8117fb9af410970934b9bb925b81ec10d879d582))

## [3.7.2](https://gitlab.com/html-validate/jest-config/compare/v3.7.1...v3.7.2) (2023-04-15)

### Bug Fixes

- should be mjs not cjs ([a1f088f](https://gitlab.com/html-validate/jest-config/commit/a1f088f1329cbca74bca1b28393523281b03774f))

## [3.7.1](https://gitlab.com/html-validate/jest-config/compare/v3.7.0...v3.7.1) (2023-04-15)

### Bug Fixes

- transform .cjs files ([f2970ad](https://gitlab.com/html-validate/jest-config/commit/f2970adaba65e841c363bc2e876823c0ec778522))

## [3.7.0](https://gitlab.com/html-validate/jest-config/compare/v3.6.3...v3.7.0) (2023-04-15)

### Features

- support using babel to transform esm in node_modules ([3cb93c1](https://gitlab.com/html-validate/jest-config/commit/3cb93c14f04b6dc708e4096ba7da19a4437b4832))

## [3.6.3](https://gitlab.com/html-validate/jest-config/compare/v3.6.2...v3.6.3) (2023-03-18)

### Dependency upgrades

- **deps:** support typescript v5 ([c99bbf5](https://gitlab.com/html-validate/jest-config/commit/c99bbf51a1de7f08d09502944e44c084a15b8dad))

## [3.6.2](https://gitlab.com/html-validate/jest-config/compare/v3.6.1...v3.6.2) (2022-11-20)

### Bug Fixes

- add bin script for easier debugging ([5beb042](https://gitlab.com/html-validate/jest-config/commit/5beb042f5b3ce6e63d9d665aa91dec6663d04781))
- config typo ([13ac5b6](https://gitlab.com/html-validate/jest-config/commit/13ac5b6094c88c6e05b20b2cb6e8ab790c797de4))

## [3.6.1](https://gitlab.com/html-validate/jest-config/compare/v3.6.0...v3.6.1) (2022-11-20)

### Bug Fixes

- add missing file to npm package ([abe1b29](https://gitlab.com/html-validate/jest-config/commit/abe1b296489eb0c177a410b6f188f91a6dcf18b7))

## [3.6.0](https://gitlab.com/html-validate/jest-config/compare/v3.5.2...v3.6.0) (2022-11-20)

### Features

- use `tsconfig.jest.json` if present ([41e63d6](https://gitlab.com/html-validate/jest-config/commit/41e63d63880bf6a3e3709c3c5eaf2b30847b764c))

## [3.5.2](https://gitlab.com/html-validate/jest-config/compare/v3.5.1...v3.5.2) (2022-11-20)

### Dependency upgrades

- **deps:** update dependency jest-junit to v15 ([5fdf1e6](https://gitlab.com/html-validate/jest-config/commit/5fdf1e6d2e40b978f3ecb8f3e2a7c383cf56575b))

## [3.5.1](https://gitlab.com/html-validate/jest-config/compare/v3.5.0...v3.5.1) (2022-09-09)

### Bug Fixes

- properly depend on `jest-serializer-ansi-escapes` ([75b9fb7](https://gitlab.com/html-validate/jest-config/commit/75b9fb75e30967dcfbbfe0b59e97fd1a66622090))

## [3.5.0](https://gitlab.com/html-validate/jest-config/compare/v3.4.1...v3.5.0) (2022-09-09)

### Features

- jest 29 support ([176c972](https://gitlab.com/html-validate/jest-config/commit/176c97253bccf1a00805ca45e9a75ea6de885ba9))

## [3.4.1](https://gitlab.com/html-validate/jest-config/compare/v3.4.0...v3.4.1) (2022-08-28)

### Dependency upgrades

- **deps:** update dependency jest-junit to v14.0.1 ([02674ab](https://gitlab.com/html-validate/jest-config/commit/02674ab0419c2f05fbdea8b720776e2592f61331))

## [3.4.0](https://gitlab.com/html-validate/jest-config/compare/v3.3.3...v3.4.0) (2022-07-08)

### Features

- support import html files as strings ([f104ad4](https://gitlab.com/html-validate/jest-config/commit/f104ad4c44ef92bff0a02ecc648bd7a2e2b78cfe))

## [3.3.3](https://gitlab.com/html-validate/jest-config/compare/v3.3.2...v3.3.3) (2022-07-03)

### Dependency upgrades

- **deps:** update dependency jest-junit to v14 ([814327e](https://gitlab.com/html-validate/jest-config/commit/814327e28676c75944c86ff68627224bedebae63))

### [3.3.2](https://gitlab.com/html-validate/jest-config/compare/v3.3.1...v3.3.2) (2022-05-26)

### Bug Fixes

- ignore generated files ([43e95c3](https://gitlab.com/html-validate/jest-config/commit/43e95c3e4ab55a729f8d6c5307f9185eddc8d3e0))

### [3.3.1](https://gitlab.com/html-validate/jest-config/compare/v3.3.0...v3.3.1) (2022-05-05)

### Bug Fixes

- support jest 25 and 26 again ([42d3f2a](https://gitlab.com/html-validate/jest-config/commit/42d3f2ae63a3ba5105e0524fa43843b0f4061d5b))

## [3.3.0](https://gitlab.com/html-validate/jest-config/compare/v3.2.0...v3.3.0) (2022-05-04)

### Features

- drop support for jest 25 and 26 ([92832f2](https://gitlab.com/html-validate/jest-config/commit/92832f2806612c2f9016ed528bb1db2c73991b4b))

### Bug Fixes

- fix ConvertAnsi plugin export ([4abf108](https://gitlab.com/html-validate/jest-config/commit/4abf108ea505187cd81556ecaf6defdc99fb0be5))

## [3.2.0](https://gitlab.com/html-validate/jest-config/compare/v3.1.0...v3.2.0) (2022-05-03)

### Features

- bundle convert-ansi plugin ([decfca1](https://gitlab.com/html-validate/jest-config/commit/decfca14d38c92e3504d003198f85ab938fde4b4))
- drop support for node 12 and jest 24 ([a864c88](https://gitlab.com/html-validate/jest-config/commit/a864c881febd372b6d29bc81b86b4c176677c2c5))
- jest 28 support ([7c46d53](https://gitlab.com/html-validate/jest-config/commit/7c46d530857717617a64fb3269b30b06da5dbde8))
- set `printBasicPrototype` to false for snapshots ([43b9f67](https://gitlab.com/html-validate/jest-config/commit/43b9f67cac93652d7df0c75cd6a247429269c621))

### Dependency upgrades

- **deps:** pin dependency jest-junit to v ([44882e1](https://gitlab.com/html-validate/jest-config/commit/44882e18da2e3919501ed7bfb38f13c25549777b))
- **deps:** update dependency jest-junit to v13.2.0 ([31e4be0](https://gitlab.com/html-validate/jest-config/commit/31e4be0b34e74eea8bea98e059307bd9f752b622))
- **deps:** update dependency pretty-format to v28 ([ecc704e](https://gitlab.com/html-validate/jest-config/commit/ecc704e73ca245c7c01e016bfebb9b1ef6c70703))

## [3.1.0](https://gitlab.com/html-validate/jest-config/compare/v3.0.2...v3.1.0) (2022-05-01)

### Features

- set `escapeString` to false for snapshots ([41bccfc](https://gitlab.com/html-validate/jest-config/commit/41bccfc3dba12de4264de07e1104c962687cd931))

### [3.0.2](https://gitlab.com/html-validate/jest-config/compare/v3.0.1...v3.0.2) (2022-04-10)

### Bug Fixes

- ignore test fixtures ([95800ad](https://gitlab.com/html-validate/jest-config/commit/95800adb67ad58eab8978a1a1acca0e4d83d0584))

### [3.0.1](https://gitlab.com/html-validate/jest-config/compare/v3.0.0...v3.0.1) (2021-10-16)

### Bug Fixes

- enable cobertura ([5221485](https://gitlab.com/html-validate/jest-config/commit/5221485482c894f3024ebf446db6655fb0837ead))

## [3.0.0](https://gitlab.com/html-validate/jest-config/compare/v2.2.0...v3.0.0) (2021-10-10)

### ⚠ BREAKING CHANGES

- **deps:** Update dependency jest-junit to v13

### Dependency upgrades

- **deps:** Update dependency jest-junit to v13 ([76cd448](https://gitlab.com/html-validate/jest-config/commit/76cd448bfe71eaa0e70b29e77a52d34a28f3ed33))

## [2.2.0](https://gitlab.com/html-validate/jest-config/compare/v2.1.0...v2.2.0) (2021-06-16)

### Features

- compatibility with older jest versions ([2f8b37b](https://gitlab.com/html-validate/jest-config/commit/2f8b37be8078f87704f9414e6b154de6481111ac))

## [2.1.0](https://gitlab.com/html-validate/jest-config/compare/v2.0.0...v2.1.0) (2021-05-31)

### Features

- convert ansi in snapshots ([158b13b](https://gitlab.com/html-validate/jest-config/commit/158b13b982404585e950799dbcb62bbeeb33515d))

### Bug Fixes

- serializer path ([1566a55](https://gitlab.com/html-validate/jest-config/commit/1566a552da12d07b98021946b458c99b104df2b1))
- use `require.resolve` to resolve dependencies ([34b34f6](https://gitlab.com/html-validate/jest-config/commit/34b34f6595ba8b846f66d74b02bb684876cd41a1))

### Dependency upgrades

- **deps:** relax dependency ranges ([d57987b](https://gitlab.com/html-validate/jest-config/commit/d57987bf51e792c842f94ce694662d5da49bc386))
- **deps:** require jest 27.x ([bdc3650](https://gitlab.com/html-validate/jest-config/commit/bdc3650c3dc5e2f091e48cc9997c51cd4aefede5))

## [2.0.0](https://gitlab.com/html-validate/jest-config/compare/v1.2.10...v2.0.0) (2021-05-28)

### ⚠ BREAKING CHANGES

- **deps:** Update dependency ts-jest to v27

### Dependency upgrades

- **deps:** update dependency jest-junit to v12.1.0 ([d99dae7](https://gitlab.com/html-validate/jest-config/commit/d99dae73b7bd7a26702f1e6972568d26c7f704e0))
- **deps:** Update dependency ts-jest to v27 ([1cffaba](https://gitlab.com/html-validate/jest-config/commit/1cffaba9c828b40bfb02f3a790bf354adf4727c5))

### [1.2.10](https://gitlab.com/html-validate/jest-config/compare/v1.2.9...v1.2.10) (2021-05-09)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.6 ([ad365c7](https://gitlab.com/html-validate/jest-config/commit/ad365c70ee6e3f90a92eee48ac3440ac4d5fa3a4))

### [1.2.9](https://gitlab.com/html-validate/jest-config/compare/v1.2.8...v1.2.9) (2021-04-18)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.5 ([c8ec4f2](https://gitlab.com/html-validate/jest-config/commit/c8ec4f2e5e0a74a6f98a41028603197a259668d6))

### [1.2.8](https://gitlab.com/html-validate/jest-config/compare/v1.2.7...v1.2.8) (2021-03-21)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.4 ([bb2ce7d](https://gitlab.com/html-validate/jest-config/commit/bb2ce7d46974d3b7f9d8fa09be3d4fb9710995d6))

### [1.2.7](https://gitlab.com/html-validate/jest-config/compare/v1.2.6...v1.2.7) (2021-03-07)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.3 ([ad9c2a1](https://gitlab.com/html-validate/jest-config/commit/ad9c2a1a7ae2314520efdc1f3becd020d3624d2f))

### [1.2.6](https://gitlab.com/html-validate/jest-config/compare/v1.2.5...v1.2.6) (2021-02-28)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.2 ([ed174cf](https://gitlab.com/html-validate/jest-config/commit/ed174cf05ca2f75b504953bb1a20b303feaf0eb9))

### [1.2.5](https://gitlab.com/html-validate/jest-config/compare/v1.2.4...v1.2.5) (2021-02-14)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.1 ([50532ab](https://gitlab.com/html-validate/jest-config/commit/50532abbb48fe8d1c66a2261590929ebb02f58ce))

### [1.2.4](https://gitlab.com/html-validate/jest-config/compare/v1.2.3...v1.2.4) (2021-01-31)

### Dependency upgrades

- **deps:** update dependency ts-jest to v26.5.0 ([484f358](https://gitlab.com/html-validate/jest-config/commit/484f358ba5760b8f9a1f84c48597d6dfa694803d))

### [1.2.3](https://gitlab.com/html-validate/jest-config/compare/v1.2.2...v1.2.3) (2020-12-19)

### Bug Fixes

- fix monorepo regexp ([eb8ecec](https://gitlab.com/html-validate/jest-config/commit/eb8ecec013243fd25ad12d2e224b90a6f7466a9a))

### [1.2.2](https://gitlab.com/html-validate/jest-config/compare/v1.2.1...v1.2.2) (2020-12-19)

### Bug Fixes

- coverage in monorepo layout ([3ae96ab](https://gitlab.com/html-validate/jest-config/commit/3ae96ab77d4f4836b7b30249676c4e244ceccfba))

### [1.2.1](https://gitlab.com/html-validate/jest-config/compare/v1.2.0...v1.2.1) (2020-12-19)

### Bug Fixes

- ignore compiled specs in monorepo layout ([51641c0](https://gitlab.com/html-validate/jest-config/commit/51641c0d04031c7073f6e6648e525d520c46be92))

## [1.2.0](https://gitlab.com/html-validate/jest-config/compare/v1.1.0...v1.2.0) (2020-11-29)

### Features

- include `ts-jest` dependency ([bcdf5fd](https://gitlab.com/html-validate/jest-config/commit/bcdf5fd8b8b41b62ab0a53ef4f13cb4eb1648210))

## [1.1.0](https://gitlab.com/html-validate/jest-config/compare/v1.0.34...v1.1.0) (2020-11-22)

### Features

- ignore tests from cypress folder ([1bc0344](https://gitlab.com/html-validate/jest-config/commit/1bc03443882fddb2c37d7e85f533352d10b49bc5))

### [1.0.34](https://gitlab.com/html-validate/jest-config/compare/v1.0.33...v1.0.34) (2020-11-14)

### Bug Fixes

- coverage report as json and lcov ([5a15f45](https://gitlab.com/html-validate/jest-config/commit/5a15f4528e7b502aad6a868cad8e88ed34fd97ad))
- ignore coverage for `index.[jt]s` ([2cb7c3d](https://gitlab.com/html-validate/jest-config/commit/2cb7c3d8035a94a1efb721c03914af514b33d154))

### [1.0.33](https://gitlab.com/html-validate/jest-config/compare/v1.0.32...v1.0.33) (2020-11-05)

### Bug Fixes

- ignore `dist` folder ([24d4030](https://gitlab.com/html-validate/jest-config/commit/24d4030799826f9354939388b5feb79c1f5669e3))

## [1.0.32](https://gitlab.com/html-validate/jest-config/compare/v1.0.31...v1.0.32) (2020-11-01)

## [1.0.31](https://gitlab.com/html-validate/jest-config/compare/v1.0.30...v1.0.31) (2020-10-25)

## [1.0.30](https://gitlab.com/html-validate/jest-config/compare/v1.0.29...v1.0.30) (2020-10-18)

## [1.0.29](https://gitlab.com/html-validate/jest-config/compare/v1.0.28...v1.0.29) (2020-10-11)

### Bug Fixes

- **deps:** update dependency jest-junit to v12 ([c99067b](https://gitlab.com/html-validate/jest-config/commit/c99067ba9be2519fca8a7cd1a353cc50fc1469fa))

## [1.0.28](https://gitlab.com/html-validate/jest-config/compare/v1.0.27...v1.0.28) (2020-10-04)

## [1.0.27](https://gitlab.com/html-validate/jest-config/compare/v1.0.26...v1.0.27) (2020-09-27)

## [1.0.26](https://gitlab.com/html-validate/jest-config/compare/v1.0.25...v1.0.26) (2020-09-20)

## [1.0.25](https://gitlab.com/html-validate/jest-config/compare/v1.0.24...v1.0.25) (2020-09-13)

## [1.0.24](https://gitlab.com/html-validate/jest-config/compare/v1.0.23...v1.0.24) (2020-09-06)

## [1.0.23](https://gitlab.com/html-validate/jest-config/compare/v1.0.22...v1.0.23) (2020-08-30)

## [1.0.22](https://gitlab.com/html-validate/jest-config/compare/v1.0.21...v1.0.22) (2020-08-23)

## [1.0.21](https://gitlab.com/html-validate/jest-config/compare/v1.0.20...v1.0.21) (2020-08-16)

## [1.0.20](https://gitlab.com/html-validate/jest-config/compare/v1.0.19...v1.0.20) (2020-08-09)

## [1.0.19](https://gitlab.com/html-validate/jest-config/compare/v1.0.18...v1.0.19) (2020-08-02)

### Bug Fixes

- **deps:** update dependency jest-junit to v11.1.0 ([b16824e](https://gitlab.com/html-validate/jest-config/commit/b16824ead54e91ccf4ddd263ba64894fe8984130))

## [1.0.18](https://gitlab.com/html-validate/jest-config/compare/v1.0.17...v1.0.18) (2020-07-26)

## [1.0.17](https://gitlab.com/html-validate/jest-config/compare/v1.0.16...v1.0.17) (2020-07-19)

## [1.0.16](https://gitlab.com/html-validate/jest-config/compare/v1.0.15...v1.0.16) (2020-07-12)

## [1.0.15](https://gitlab.com/html-validate/jest-config/compare/v1.0.14...v1.0.15) (2020-07-05)

## [1.0.14](https://gitlab.com/html-validate/jest-config/compare/v1.0.13...v1.0.14) (2020-06-28)

### Bug Fixes

- **deps:** update dependency jest-junit to v11 ([bdfefdc](https://gitlab.com/html-validate/jest-config/commit/bdfefdc1f69f8c64468c93473d3f555bc913ef8d))

## [1.0.13](https://gitlab.com/html-validate/jest-config/compare/v1.0.12...v1.0.13) (2020-06-21)

## [1.0.12](https://gitlab.com/html-validate/jest-config/compare/v1.0.11...v1.0.12) (2020-06-14)

## [1.0.11](https://gitlab.com/html-validate/jest-config/compare/v1.0.10...v1.0.11) (2020-06-07)

## [1.0.10](https://gitlab.com/html-validate/jest-config/compare/v1.0.9...v1.0.10) (2020-05-31)

## [1.0.9](https://gitlab.com/html-validate/jest-config/compare/v1.0.8...v1.0.9) (2020-05-24)

## [1.0.8](https://gitlab.com/html-validate/jest-config/compare/v1.0.7...v1.0.8) (2020-05-17)

## [1.0.7](https://gitlab.com/html-validate/jest-config/compare/v1.0.6...v1.0.7) (2020-05-10)

## [1.0.6](https://gitlab.com/html-validate/jest-config/compare/v1.0.5...v1.0.6) (2020-05-03)

## [1.0.5](https://gitlab.com/html-validate/jest-config/compare/v1.0.4...v1.0.5) (2020-04-26)

## [1.0.4](https://gitlab.com/html-validate/jest-config/compare/v1.0.3...v1.0.4) (2020-04-19)

### Bug Fixes

- drop support for jsx and `moduleFileExtensions` ([756eea5](https://gitlab.com/html-validate/jest-config/commit/756eea52dd2dc8d2da95fd144374b80dc424bc5e))

## [1.0.3](https://gitlab.com/html-validate/jest-config/compare/v1.0.2...v1.0.3) (2020-04-19)

### Bug Fixes

- always ignore build folder ([4c8a5b0](https://gitlab.com/html-validate/jest-config/commit/4c8a5b01d857908b69c5834db49179301eaa746b))

## [1.0.2](https://gitlab.com/html-validate/jest-config/compare/v1.0.1...v1.0.2) (2020-04-19)

### Bug Fixes

- allow `spec.js` ([3172972](https://gitlab.com/html-validate/jest-config/commit/31729725c60081cdd821c2ba33cc71867808bf0f))

## [1.0.1](https://gitlab.com/html-validate/jest-config/compare/v1.0.0...v1.0.1) (2020-04-19)

### Bug Fixes

- allow **tests** too ([462029e](https://gitlab.com/html-validate/jest-config/commit/462029ec35a0faa3ad7ec068853b056d97656256))

# 1.0.0 (2020-04-19)

### Bug Fixes

- dont run coverage on d.ts ([5a2c463](https://gitlab.com/html-validate/jest-config/commit/5a2c4632905f802f2851f7f505b3f7a406369651))

### Features

- initial version ([178dee8](https://gitlab.com/html-validate/jest-config/commit/178dee8d6378b9f2eb07532bff48e5d266b3202a))
