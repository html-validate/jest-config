# @html-validate/jest-config

> HTML-Validate jest preset.

Jest is configured to:

- Run tests from `*.spec.[jt]s`.
- Collect coverage from `.js` and `.ts` in `src` and ignore tests (`**/*.spec.[jt]s`) and index (`**/index.[jt]s`).
- Report results as junit in `temp/jest.xml`.
- Transform typescript with `ts-jest`.
- Transform js with `babel-jest` if babel is detected.
- Ignores spec-files from Cypress.

## Install

```
npm install --save-dev @html-validate/jest-config
```

## Usage

In your `package.json` file:

```json
{
  "jest": {
    "preset": "@html-validate/jest-config"
  }
}
```

### Presets

| Preset                                     | Description               |
| ------------------------------------------ | ------------------------- |
| @html-validate/jest-config                 | Alias for presets/default |
| @html-validate/jest-config/presets/default | Default preset            |
| @html-validate/jest-config/presets/esm     | Experimental ESM support  |

### Transpiling ESM in `node_modules/`

If you need to import and use ESM-only libraries you need to install `@babel/core.
You do not need a `babel.config.js`or`.babelrc`as the preset passes the required configuration directly to`babel-jest`.

```
npm install --save-dev @babel/core
```

When babel is detect the preset sets `transformIgnorePatterns` to transpile everything under `node_modules`.
You can include a more specific list of packages to transform (potential speed gain) by manually setting `transformIgnorePatterns`:

Optionally you can set `transformIgnorePatterns` in `package.json`:

```diff
 {
   "jest": {
     "preset": "@html-validate/jest-config",
+    "transformIgnorePatterns": ["node_modules/(?!(foo|bar|baz)/)"]
   }
 }
```

This will transform only the `foo`, `bar` and `baz` packages.

### Importing HTML as string

Importing `*.html` is supported by jest configuration but requires additional typescript config by creating a declaration:

`shim-html.d.ts`:

```ts
declare module "*.html" {
  const value: string;
  export default value;
}
```

### `tsconfig.json`

This preset searches the project folder for typescript configuration in the following order:

1. `tsconfig.jest.json`
2. `tsconfig.json`
